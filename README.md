# WurstWorks XNAT Cthulhu Branding Plugin #

The WurstWorks XNAT Cthulhu Branding Plugin is a simple example of a branding plugin for XNAT. It provides:

 * [An alternate logo for use in XNAT](src/main/resources/META-INF/resources/images/cthulhu.jpg)
 * [A custom site description contained in a Velocity template](src/main/resources/META-INF/resources/templates/screens/cthulhu-site.vm)

## Building the Plugin ##

To build the Cthulhu branding plugin jar:

```bash
./gradlew jar
```

The resulting plugin can be found in a file named **cthulhu-branding-_version_.jar** in the folder **build/libs**.

## Installing the Plugin ##

Instructions on installing XNAT plugins can be found [on the XNAT documentation site](https://wiki.xnat.org/documentation/xnat-developer-documentation/working-with-xnat-plugins/developing-xnat-plugins).

